package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;


public class Divide extends AbstractOperation implements BinaryOperation{
	/**
	 * Binary Divide operation
	 * 
	 */
// Added the Division operator to this class
		@Override
		public float perform(float arg1, float arg2) {
			return arg1 / arg2;
		}

		@Override
		public String getName() {
			return "/";
		}

}
